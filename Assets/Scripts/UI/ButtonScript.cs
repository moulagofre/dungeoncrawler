﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScript : MonoBehaviour {

    private string newScene;

    // List of functions
    public enum FunctionsList
    {
        play,
        showScores,
        quit,
        back,
        credits,
    }

    [SerializeField] FunctionsList action;

    public void OnClick()
    {
        // get previous scene name for "back" button
        newScene = PlayerPrefs.GetString("previousScene");
        // set new "previous scene name"
        PlayerPrefs.SetString("previousScene", SceneManager.GetActiveScene().name);

        // Check button action
        switch (action)
        {
            case FunctionsList.play:
                SceneManager.LoadScene("Sandbox");
                break;
            case FunctionsList.showScores:
                break;
            case FunctionsList.quit:
                Application.Quit();
                break;
            case FunctionsList.back:
                SceneManager.LoadScene(newScene);
                break;
            case FunctionsList.credits:
                SceneManager.LoadScene("Credits");
                break;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour {

    private bool interact = false;
    private bool canInteract = false;
    private float interactionRadius;

    [SerializeField] private Transform interactCheck;
    [SerializeField] private SpriteRenderer refObject;
    [SerializeField] private LayerMask whatIsInteractive;

    private GameObject lastObject;


    // Use this for initialization
    void Start () {
		
	}

    // Update is called once per frame
    void Update()
    {
        // Get input
        if (Input.GetButtonDown("Interact") && canInteract)
        {
            interact = true;
        }

    }

    void FixedUpdate () {

        interactionRadius = refObject.size.y >= refObject.size.x ? refObject.size.y : refObject.size.x;

        if (Physics2D.OverlapCircle(interactCheck.position, interactionRadius, whatIsInteractive))
        {
            //Save last detected object (for disabling)
            lastObject = Physics2D.OverlapCircle(interactCheck.position, interactionRadius, whatIsInteractive).gameObject;
            //Show input indicator
            if (!canInteract)
            {
                gameObject.SendMessage("SetVisibility");
            }
            canInteract = true;

            //Trigger interaction action
            if (interact)
            {
                //actions
                gameObject.SendMessage("Interaction");
                interact = false;
            }
        }
        else
        {
            //Hide input indicator
            if (canInteract)
            {
                gameObject.SendMessage("SetVisibility");
            }
            canInteract = false;
        }
    }

}

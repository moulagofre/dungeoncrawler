﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightMask : MonoBehaviour {

    [Range(0.01f, 0.2f)]
    [SerializeField] float flickTime;

    [Range(0.01f, 0.2f)]
    [SerializeField] float addSize;

    [SerializeField] Transform target;

    float timer = 0;
    private bool bigger = true;

    // Update is called once per frame
    void Update () {

        addSize = transform.localScale.x / 100;

        timer += Time.deltaTime;
        if(timer > flickTime)
        {
            if (bigger)
            {
                transform.localScale = new Vector3(transform.localScale.x + addSize,
                    transform.localScale.y + addSize, transform.localScale.z);
            }
            else
            {
                transform.localScale = new Vector3(transform.localScale.x - addSize,
                    transform.localScale.y - addSize, transform.localScale.z);
            }

            timer = 0;
            bigger =! bigger;
        }

        if (target != null)
        {
            //transform.position = Vector3.MoveTowards(transform.position, target.position, 20 * Time.deltaTime);
            transform.position = target.position;
        }
	}

    public void setTarget(Transform targetToSet)
    {
        target = targetToSet;
    }

    public bool getState()
    {
        return target.gameObject.GetComponent<HandleLightMask>().getIsOn();
    }
}

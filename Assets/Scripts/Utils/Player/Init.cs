﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Init : MonoBehaviour {

    [SerializeField] private PlayerInfo playerInfoPrefab;

    [SerializeField] private LightMask playerLightMaskPrefab;

    private LightMask lightmask;

    private PlayerInfo playerInfo;

    // Use this for initialization
    void Awake () {

        // Instantiate lightmask
        lightmask = Instantiate(playerLightMaskPrefab);
        lightmask.setTarget(transform);
        lightmask.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        lightmask.transform.SetParent(GameObject.FindGameObjectWithTag("LightMasks").transform);

        // Intantiate player Health and Stamina Bars
        playerInfo = Instantiate(playerInfoPrefab);
        playerInfo.setTarget(transform);
        playerInfo.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        playerInfo.transform.SetParent(GameObject.FindGameObjectWithTag("UIOverlay").transform);

        // Set player health and stamina bar for ui updating
        gameObject.GetComponent<FightSystemController>().setHealthBar(playerInfo.getHealthBar());
        gameObject.GetComponent<FightSystemController>().setStaminaBar(playerInfo.getStaminaBar());
    }
	
}

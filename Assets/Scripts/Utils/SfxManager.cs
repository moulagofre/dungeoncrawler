﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SfxManager : MonoBehaviour {

    [SerializeField] AudioClip[] Clip;

    [SerializeField] [Range(0f, 5f)] float[] Delay;

    [SerializeField] AudioSource Source;

    private int currentId;

    public void setSound(int id)
    {
        currentId = id;
        Source.clip = Clip[currentId];
    }

    public void playSound()
    {
        Source.PlayDelayed(Delay[currentId]);
    }
}

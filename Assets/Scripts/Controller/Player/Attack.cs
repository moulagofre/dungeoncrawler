﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack : MonoBehaviour {

    private bool isAttacking; //Is player attacking or not
    private bool attack = false;
    private bool draw = false;
    private float sheatheTime;
    private float timeBeforeAttack;
    private int currentAttack = 1;

    [SerializeField] private float timeBeforeSheate = 1;
    [SerializeField] private float attackSpeed;
    [SerializeField] CharacterController2D controller;
    [SerializeField] Animator animator;
    [SerializeField] SfxManager sfx;
    [SerializeField] float attack2BumpForce;
    [SerializeField] float attack3BumpForce;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetButtonDown("Attack") && controller.getGrounded())
        {
            attack = true;
        }
    }

    void FixedUpdate()
    {
        HandleDraw();
        HandleAttacks();
    }

    private void HandleAttacks()
    {
        if (currentAttack > 3)
        {
            currentAttack = 1;
        }

        if (attack)
        {
            if (draw)
            {
                if (controller.getGrounded() && !controller.getFalling() && timeBeforeAttack <= 0)
                {
                    Debug.Log("Attack !"+currentAttack);
                    animator.SetTrigger("attack"+currentAttack);
                    sfx.setSound(currentAttack);
                    sfx.playSound();
                    switch (currentAttack)
                    {
                        case 2:
                            controller.Move(controller.getDirection() * attack2BumpForce, false, false, false, false);
                            Debug.Log(controller.getDirection() * attack2BumpForce);
                            break;
                        case 3:
                            controller.Move(controller.getDirection() * attack3BumpForce, false, false, false, false);
                            break;
                        default:
                            break;
                    }
                    currentAttack += 1;
                    timeBeforeAttack = 1 / attackSpeed;
                }
                attack = false;
            }
            resetTimer();
        }
        if (draw ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("player_sheathe") ||
            animator.GetCurrentAnimatorStateInfo(0).IsName("player_idle_draw")
            )
        {
            isAttacking = true;
        }else
        {
            isAttacking = false;
        }

        timeBeforeAttack -= Time.deltaTime;
    }

    private void HandleDraw()
    {
        if (attack && !draw)
        {
            animator.ResetTrigger("sheathe");
            animator.SetTrigger("draw");
            sfx.setSound(0);
            sfx.playSound();
            draw = true;
            resetTimer();
            attack = false;
        }

        sheatheTime -= Time.deltaTime;

        if (draw && sheatheTime <= 0.0f)
        {
            animator.SetTrigger("sheathe");
            draw = false;
            sfx.setSound(4);
            sfx.playSound();
            currentAttack = 1;
        }

    }

    private void resetTimer()
    {
        sheatheTime = timeBeforeSheate; 
    }

    public bool getIsAttacking()
    {
        return isAttacking;
    }

}



